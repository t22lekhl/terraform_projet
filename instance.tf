resource "openstack_compute_instance_v2" "bastion" {
  name            = "bastion"
  image_id        = "00000000-0000-0000-0001-000000000103"
  flavor_id       = "3"
  key_pair        = "demo"
  security_groups = ["${openstack_compute_secgroup_v2.secgroup_bastion.name}"]



  network {
    name = var.network_name
  }
  provisioner "local-exec" {
    command = "echo '${self.name} : ${self.access_ip_v4}' >> ~/Bureau/terraform_projet/adresses_IP.txt"


  }



}

resource "openstack_networking_floatingip_v2" "fip_admin" {
  pool = var.pool_ext
}
resource "openstack_compute_floatingip_associate_v2" "fip_admin" {
  floating_ip = "${openstack_networking_floatingip_v2.fip_admin.address}"
  instance_id = "${openstack_compute_instance_v2.bastion.id}"
  depends_on = [
  openstack_networking_router_v2.internal_router_1,
  openstack_compute_secgroup_v2.secgroup_bastion,
  openstack_networking_subnet_v2.subnet_sub
  ]
}

resource "openstack_compute_instance_v2" "node" {
  for_each = toset (["node01","node02","node03"])
  name = each.key
  image_name = "ubuntu-20.04"
  flavor_name = "m1.small"
  network {
    name = "network_net"
  }
    provisioner "local-exec" {
    command = "echo '${self.name} : ${self.access_ip_v4}' >> ~/Bureau/terraform_projet/adresses_IP.txt"


  }
  security_groups = ["${openstack_compute_secgroup_v2.secgroup_application.name}"]
}
resource "openstack_networking_floatingip_v2" "fip_application" {
  pool = var.pool_ext
}
resource "openstack_compute_floatingip_associate_v2" "fip_application" {
  floating_ip = "${openstack_networking_floatingip_v2.fip_application.address}"
  instance_id = "${openstack_compute_instance_v2.node["node01"].id}"
  depends_on = [openstack_networking_router_v2.internal_router_1]
}


