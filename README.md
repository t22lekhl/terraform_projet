# Descriptif du projet

Auteur : Lekhlaikh Taha

Contact : taha.lekhlaikh@imt-atlantique.net

Formation : Master Spécialisé Infrastructures Cloud et DevOps

Ce projet a pour but de mettre en place une infrastructure Cloud sur Openstack grâce à Terraform qui contient :
un réseau, un sous-réseau, une instance bastion, 3 instances d'orchestration "node", IP flottante, groupes de sécurité, un fichier "adress_IP.txt" contenant les IP locales des instances, généré automatiquement.



# Clone du projet : 

git clone https://gitlab.imt-atlantique.fr/t22lekhl/terraform_projet



# Configuration avec Openstack :

cd terraform_projet

source os-openrc.sh

env | grep OS_

Le fichier "os-openrc.sh" correspond à votre fichier RC personnel.


# Déploiement de l'architecture

terraform init

terraform plan

terraform apply

# Résultat : 

Creation d'un fichier adress_IP qui contient les adresses IP internes des instances déployées

# Suppression de l'architecture

terraform destroy
