resource "openstack_compute_secgroup_v2" "secgroup_bastion" {
  name        = "secgroup_bastion"
  description = "secgroup_bastion"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = var.cidr_meta
  }

}
resource "openstack_compute_secgroup_v2" "secgroup_application" {
  name        = "secgroup_application"
  description = "secgroup_application"

  rule {
    from_port   = 80
    to_port     = 80
    ip_protocol = "tcp"
    cidr        = var.cidr_meta
  }

}
resource "openstack_compute_secgroup_v2" "secgroup_reseau_interne" {
  name        = "secgroup_reseau_interne"
  description = "secgroup_reseau_interne"

    rule {
    from_port   = 1
    to_port     = 65535
    ip_protocol = "udp"
    cidr        = var.cidr_meta
  }
    rule {
    from_port   = 1
    to_port     = 65535
    ip_protocol = "tcp"
    cidr        = var.cidr_meta
  }


}
