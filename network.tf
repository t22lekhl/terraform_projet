resource "openstack_networking_network_v2" "network_net" {
  name           = var.network_name
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_sub" {
  name       = "subnet_sub"
  network_id = "${openstack_networking_network_v2.network_net.id}"
  cidr       = var.subnet_ip_range
  ip_version = 4
}

