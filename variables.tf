variable "network_name" {
  type    = string
  default = "network_net"
}
variable "subnet_ip_range" {
  type    = string
  default = "192.168.1.0/24"
}
variable "cidr_meta" {
  type    = string
  default = "0.0.0.0/0"
}
variable "pool_ext" {
  type    = string
  default = "external"
}
variable "external_id" {
  type    = string
  default = "7af4393b-6556-4d29-b68e-107d02709b96"
}
