resource "openstack_networking_router_interface_v2" "internal_router_interface_1" {
  router_id = "${openstack_networking_router_v2.internal_router_1.id}"
  subnet_id = "${openstack_networking_subnet_v2.subnet_sub.id}"
}

